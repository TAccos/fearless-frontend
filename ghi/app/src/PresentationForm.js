import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [presenterName, setPresenterName] = useState("");
    const [presenterEmail, setPresenterEmail] = useState("");
    const [companyName, setCompanyName] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState("");

    const handlePresenterNameChange = (e) => {
        const value = e.target.value;
        setPresenterName(value);
    };
    const handlePresenterEmailChange = (e) => {
        const value = e.target.value;
        setPresenterEmail(value);
    };
    const handleCompanyNameChange = (e) => {
        const value = e.target.value;
        setCompanyName(value);
    };
    const handleTitleChange = (e) => {
        const value = e.target.value;
        setTitle(value);
    };
    const handleSynopsisChange = (e) => {
        const value = e.target.value;
        setSynopsis(value);
    };
    const handleConferenceChange = (e) => {
        const value = e.target.value;
        setConference(value);
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();

            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        };
    };

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

return (
    <>
    <div className='row'>
    <div className='offset-3 col-6'>
            <div className='shadow p-4 mt-4'>
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className='form-floating mb-3'>
                <input value={presenterName}
                onChange={handlePresenterNameChange}
                placeholder='Presenter name'
                required type='text'
                name='presenter-name'
                id='presenter-name'
                className='form-control'/>
                <label htmlFor='presenter-name'>Name</label>
            </div>
            <div className="form-floating mb-3">
                <input value={presenterEmail}
                onChange={handlePresenterEmailChange}
                placeholder="Presenter email"
                required type="text"
                name="presenter-email"
                id="presenter-email"
                className="form-control"/>
                <label htmlFor="presenter-email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
                <input value={companyName}
                onChange={handleCompanyNameChange}
                placeholder="Company name"
                required type="text"
                name="company-name"
                id="company-name"
                className="form-control"/>
                <label htmlFor="company-name">Company name</label>
            </div>
            <div className="form-floating mb-3">
                <input value={title}
                onChange={handleTitleChange}
                placeholder="Title"
                required type="text"
                name="title"
                id="title"
                className="form-control"/>
                <label htmlFor="title">Title</label>
            </div>
            <div className="form-floating mb-3">
                <input value={synopsis}
                onChange={handleSynopsisChange}
                placeholder="Synopsis"
                required type="text"
                name="synopsis"
                id="synopsis"
                className="form-control"/>
                <label htmlFor="synopsis">Synopsis</label>
            </div>
            <div className='mb-3'>
            <select value={conference}
            onChange={handleConferenceChange}
            required name="conference"
            id="conference"
            className="form-select">
                    <option>Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option key={conference.name} value={conference.id}>
                                {conference.name}
                            </option>
                        );
                    })};
            </select>
            </div>
            <button className='btn btn-primary'>Create</button>
            </form>
            </div>
    </div>
    </div>
    </>
  );
}

export default PresentationForm;
